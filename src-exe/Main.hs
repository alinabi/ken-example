{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Main where

import System.Environment ( getArgs )
import Data.Text.Lazy ( Text )
import qualified Data.Text.Lazy as T 
    ( lines
    , splitOn
    , strip
    , concat
    , intercalate
    , pack
    )
import Data.Text.Lazy.IO ( readFile, writeFile )
import Data.Text.Lazy.Read ( signed, decimal )
import Prelude hiding ( readFile, writeFile )
import Data.List ( sortBy )
import Data.Ord ( comparing )


main ∷ IO ()
main = do
    args ← getArgs
    mainAction args

mainAction ∷ [String] → IO ()
mainAction [in_file, out_file] = do
    inp ← fmap decode $ readFile in_file
    let out = aggregate $ sortBy (comparing rec'date) inp
    writeFile out_file (encode out)    
mainAction _other_args = error "invalid command line arguments"


data Record = Rec 
    { rec'date  ∷ !Text
    , rec'value ∷ !Int 
    }


decode ∷ Text → [Record] 
decode = zipWith toRec [1..] . rawFields where

    abort line cause = error msg where
        msg = cause ++ " in line " ++ show line

    rawFields = map (map T.strip) . map (T.splitOn ",") . T.lines 

    toRec ∷ Int → [Text] → Record 
    toRec line [date, txt] = case signed decimal txt of
        Right (value, "") → Rec date value
        _otherwise → abort line "invalid value" 
    toRec line _other_list = abort line "ivalid record length"


encode ∷ [Record] → Text
encode = T.intercalate "\n" . map enc where
    enc (Rec date value) = T.concat [date, ",", T.pack $ show value]


aggregate ∷ [Record] → [Record] 
aggregate = scanl1 acc where
  acc (Rec _ val₁) (Rec date val₂) = Rec date (val₁ + val₂)


