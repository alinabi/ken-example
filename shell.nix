with import (<nixpkgs>) {};

let

    ghcVersion = "ghc8101";

    hsPkgs = haskell.packages.${ghcVersion};

    haskellEnv = hsPkgs.ghcWithPackages(ps: with ps; [
        cabal-install
        cabal2nix
    ]);
in
    pkgs.mkShell {
        buildInputs = [ 
	    haskellEnv 
	    llvm 
        ];
    }
